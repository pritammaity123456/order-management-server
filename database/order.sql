-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 23, 2019 at 04:30 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `order`
--

-- --------------------------------------------------------

--
-- Table structure for table `md_district`
--

CREATE TABLE `md_district` (
  `district_code` int(10) NOT NULL,
  `district_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_district`
--

INSERT INTO `md_district` (`district_code`, `district_name`) VALUES
(1, 'OTHERS'),
(2, 'MALDAH'),
(3, 'BURDWAN'),
(4, 'KOLKATA'),
(5, 'ALIPURDUAR'),
(6, 'DAKHSHIN DINAJPUR'),
(7, 'WEST MIDNAPORE'),
(8, 'HOWRAH'),
(9, 'HOOGHLY'),
(10, 'MURSHIDABAD'),
(11, 'NORTH 24 PARGANAS'),
(12, 'EAST MIDNAPORE'),
(13, 'UTTAR DINAJPUR'),
(14, 'NADIA'),
(15, 'BANKURA'),
(16, 'PURULIA'),
(17, 'COOCHBEHAR'),
(18, 'BIRBHUM'),
(19, 'DARJEELING'),
(20, 'JALPAIGURI'),
(21, 'SOUTH 24 PARGANAS');

-- --------------------------------------------------------

--
-- Table structure for table `md_project_type`
--

CREATE TABLE `md_project_type` (
  `type_cd` int(11) NOT NULL,
  `type_desc` varchar(50) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_dt` datetime NOT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_dt` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_project_type`
--

-- --------------------------------------------------------

--
-- Table structure for table `md_users`
--

CREATE TABLE `md_users` (
  `emp_id` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_status` char(1) NOT NULL DEFAULT 'A',
  `user_type` char(2) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `md_users`
--

INSERT INTO `md_users` (`emp_id`, `user_id`, `password`, `user_name`, `user_status`, `user_type`, `created_by`, `created_dt`, `modified_by`, `modified_dt`) VALUES
('pk', 'admin', '$2a$10$84rAFVVz6wnOY5Y5lMAq6eVPT8mQ1QCHhVNN/ZWeSiI0XCHdEvw4a', 'Pritam Maity', 'A', 'A', NULL, NULL, 'SELF', '2019-03-29 12:19:24'),
('kp', 'user', '$2a$10$84rAFVVz6wnOY5Y5lMAq6eVPT8mQ1QCHhVNN/ZWeSiI0XCHdEvw4a', 'Pritam Maity', 'A', 'E', NULL, NULL, 'SELF', '2019-03-29 12:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `td_project_details`
--

CREATE TABLE `td_project_details` (
  `id` int(11) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `project_type` int(11) DEFAULT NULL,
  `contact_person` varchar(50) DEFAULT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `dist` int(11) NOT NULL,
  `block` varchar(20) DEFAULT NULL,
  `order_dt` date NOT NULL,
  `order_dtls` varchar(50) DEFAULT NULL,
  `order_value` decimal(10,2) DEFAULT '0.00',
  `tax` varchar(20) DEFAULT NULL,
  `payment_terms` varchar(50) DEFAULT NULL,
  `monthly_rental` varchar(50) DEFAULT NULL,
  `payment_status` varchar(50) DEFAULT NULL,
  `proposed_instl_dt` date DEFAULT NULL,
  `sales_person` varchar(50) DEFAULT NULL,
  `installed_by` varchar(50) DEFAULT NULL,
  `installation_dt` date DEFAULT NULL,
  `online_dt` date DEFAULT NULL,
  `sss_remarks` text,
  `cust_remarks` text,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `modified_dt` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_project_details`
--


--
-- Indexes for table `md_district`
--
ALTER TABLE `md_district`
  ADD PRIMARY KEY (`district_code`);

--
-- Indexes for table `md_project_type`
--
ALTER TABLE `md_project_type`
  ADD PRIMARY KEY (`type_cd`);

--
-- Indexes for table `md_users`
--
ALTER TABLE `md_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `td_project_details`
--
ALTER TABLE `td_project_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `md_district`
--
ALTER TABLE `md_district`
  MODIFY `district_code` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `md_project_type`
--
ALTER TABLE `md_project_type`
  MODIFY `type_cd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `td_project_details`
--
ALTER TABLE `td_project_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
